# Markdown Blogger

## All You Need is Markdown

Generate a blog from your README.md file.

Simply create a README.md that contains the following:

```markdown
# Blog Name

- [Post Image](<post-image-url>) | [Post Title](<post-url>)
- []() | [Second Post](<another-url>)
```

#### IMPORTANT!

The links may contain images; however, if you don't have an image, leave the image links empty. Furthermore, notice how images do not contain "!" before the link. This is so your README.md isn't filled with loaded images.

Use a fully qualified link to your documents and images to prevent errors in the blog. A fully qualified link looks like this: `https://gitlab.com/<USERNAME>/<REPO>/-/raw/<BRANCH>/markdown/<MY-POST>.md`

## Use Version Control

If you are using Gitlab or github, open the README.md file as a raw file and use the direct link in the field to generate a blog.

For instance, `https://gitlab.com/<USERNAME>/<BLOG-REPO-NAME>/-/raw/<BRANCH>/README.md` is a valid URL.

## Tehnologies

- Rust
- Actix-Web
- Handlebars.js
- HTMX

## Source Code

Check the `Cargo.toml` file in the root directory to gain a sense of the dependencies. 
The `src` directory is kept as simple as possible.
