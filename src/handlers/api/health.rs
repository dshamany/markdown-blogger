use actix_web::{get, HttpResponse};

#[get("")] // ROUTE: /healthcheck
pub async fn check() -> Result<HttpResponse, actix_web::Error> {
    Ok(HttpResponse::Ok().body("success".to_string()))
}
