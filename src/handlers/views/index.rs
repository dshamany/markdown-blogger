use actix_web::{
    get,
    web::{self, Query},
    HttpResponse, Responder,
};
use regex::Regex;
use serde_json::json;
use std::env;

use crate::markdown_util::{self, extract_by_pattern, extract_n_match, links_as_objects};
use handlebars::Handlebars;

#[derive(serde::Deserialize)]
pub struct MarkdownUrl {
    url: String,
}

#[get("/index")]
async fn index(hb: web::Data<Handlebars<'_>>) -> impl Responder {
    let url = env::var("BLOG_URL").unwrap_or("".to_string());

    let table_of_content = markdown_util::get_markdown_document(url.as_str()).await;

    if let Ok(toc) = table_of_content {
        // Extract Blog Title
        let re_title = Regex::new("# (.*)").unwrap();
        let title =
            extract_n_match(&re_title, toc.as_str(), 1).unwrap_or("Untitled Blog".to_string());

        // Extract links with images
        let pattern = r"\[(.*)\]\((.*)\).*|.*\[(.*)\]\((.*)\)";
        let links = extract_by_pattern(pattern, toc.as_str());
        let links = links_as_objects(&links);
        let data = json!({
            "title": title,
            "articles": &links,
        });

        let view = hb.render("home", &data).unwrap();

        HttpResponse::Ok().body(view)
    } else {
        HttpResponse::BadRequest().body("Could not load grid view")
    }
}

#[get("/")]
async fn start(hb: web::Data<Handlebars<'_>>) -> impl Responder {
    let view = hb.render("landing_page", &json!({})).unwrap();
    HttpResponse::Ok().body(view)
}

#[get("/articles")]
async fn home(hb: web::Data<Handlebars<'_>>, info: Query<MarkdownUrl>) -> impl Responder {
    let url = info.into_inner().url;

    let table_of_content = markdown_util::get_markdown_document(url.as_str()).await;

    if let Ok(toc) = table_of_content {
        // Extract Blog Title
        let re_title = Regex::new("# (.*)").unwrap();
        let title =
            extract_n_match(&re_title, toc.as_str(), 1).unwrap_or("Untitled Blog".to_string());

        // Extract links with images
        let pattern = r"\[(.*)\]\((.*)\).*|.*\[(.*)\]\((.*)\)";
        let links = extract_by_pattern(pattern, toc.as_str());
        let links = links_as_objects(&links);
        let data = json!({
            "title": title,
            "articles": &links
        });

        let view = hb.render("home", &data).unwrap();

        HttpResponse::Ok().body(view)
    } else {
        HttpResponse::BadRequest().body("Could not load grid view")
    }
}

#[get("/articles/raw")]
async fn md2html(hb: web::Data<Handlebars<'_>>, info: Query<MarkdownUrl>) -> impl Responder {
    let url = info.into_inner().url;
    let body = markdown_util::to_raw_html(url.as_str()).await;

    match body {
        Ok(html) => {
            let re_title = Regex::new(r"<h1.*>(.*).</h1>").unwrap();
            let title =
                extract_n_match(&re_title, html.as_str(), 1).unwrap_or("Untitled".to_string());

            let data = json!({
                "content": html,
                "post_title": title
            });
            let view = hb.render("raw_article_index", &data).unwrap();
            HttpResponse::Ok().body(view)
        }
        Err(e) => {
            HttpResponse::BadRequest().body(format!("<h1>Error: 400 Bad Request</h1><p>{}</p>", e))
        }
    }
}

#[get("/articles/template")]
async fn md2template(hb: web::Data<Handlebars<'_>>, info: Query<MarkdownUrl>) -> impl Responder {
    let url = info.into_inner().url;
    let body = markdown_util::to_raw_html(url.as_str()).await;

    let re_date = Regex::new(r"((\d{4}).(\d{2}).(\d{2}))").unwrap();
    let date_published = extract_n_match(&re_date, &url, 0).unwrap_or("".to_string());

    match body {
        Ok(content) => {
            let re_title = Regex::new("(<h1.*>)(.*?)(</h1>)").unwrap();
            let title =
                extract_n_match(&re_title, content.as_str(), 2).unwrap_or("Untitled".to_string());
            let data = json!({
                "content": content,
                "title": title,
                "origin": url,
                "published": date_published
            });
            let view = hb.render("article_index", &data).unwrap();
            HttpResponse::Ok().body(view)
        }
        Err(e) => {
            HttpResponse::BadRequest().body(format!("<h1>Error: 400 Bad Request</h1><p>{}</p>", e))
        }
    }
}
