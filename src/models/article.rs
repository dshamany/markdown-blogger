use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Article {
    pub image_url: Option<String>,
    pub title: Option<String>,
    pub content: Option<String>,
}
