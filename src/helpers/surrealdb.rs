use std::env::{self, VarError};

use reqwest::Client;
use serde_json::Value;

use std::collections::HashMap;

#[allow(dead_code)]
pub async fn query(query: &str) -> Value {
    let surreal_user = env::var("SURREAL_USER").unwrap();
    let surreal_pass = env::var("SURREAL_PASS").unwrap();
    let surreal_ns = env::var("SURREAL_NS").unwrap();
    let surreal_db = env::var("SURREAL_DB").unwrap();
    let surreal_url = env::var("SURREAL_URL");

    let client = Client::new();
    let query = query.to_string();
    let resp = client
        .post(base_url(surreal_url))
        .basic_auth(surreal_user, Some(surreal_pass))
        .header("NS", surreal_ns)
        .header("DB", surreal_db)
        .header("accept", "application/json")
        .body(query)
        .send()
        .await
        .expect("response failed")
        .json::<Value>()
        .await
        .expect("payload failed");
    resp
}

#[allow(dead_code)]
pub fn base_url(url: Result<String, VarError>) -> String {
    match url {
        Ok(u) => u,
        Err(_) => "https://bootupdb.fly.dev/sql".to_string(),
    }
}

#[allow(dead_code)]
pub fn set_attributes(json_string: &str) -> String {
    let json_hm = serde_json::from_str::<HashMap<String, Value>>(json_string).unwrap_or_default();
    let mut attributes = String::new();
    for key in json_hm.keys() {
        let value = json_hm[key].to_string();
        if value != "null".to_string() {
            let s = format!("{}={},", key, value);
            attributes.push_str(&s);
        }
    }
    attributes.pop(); // to remove trailing comma

    format!("{}", attributes)
}

#[allow(dead_code)]
pub fn set_query_attributes(map: &HashMap<String, Value>) -> String {
    let mut result = String::new();
    for (k, v) in map.iter() {
        let tmp = format!("{}={},", k, v);
        result.push_str(&tmp);
    }
    result.pop(); // to remove trailing comma
    result
}

#[allow(dead_code)]
pub fn add_timestamp() -> String {
    String::from("metadata.createdAt=(time::now()), metadata.updatedAt=(time::now())")
}

#[allow(dead_code)]
pub fn update_timestamp() -> String {
    String::from("metadata.updatedAt=(time::now())")
}
