#[allow(dead_code)]
pub fn content_to_paragraphs<'a>(content: &'a str, split_by: &'a str) -> Vec<&'a str> {
    let paragraphs: std::str::Split<'_, &'a str> = content.split(split_by);
    let collection = paragraphs.collect::<Vec<&'a str>>();
    return collection;
}

#[allow(dead_code)]
pub fn trim_quotation(content: &str) -> String {
    let content = content.replace("\"", "");
    content
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_text_to_paragraphs() {
        let content = "Hello, there!\nHow are you doing?";
        let result = vec!["Hello, there!", "How are you doing?"];
        assert_eq!(content_to_paragraphs(content, "\n"), result);
    }

    #[test]
    fn test_trim_quotations() {
        let content = "\"This line is not in quotes\"";
        let result = "This line is not in quotes".to_string();
        assert_eq!(trim_quotation(content), result);
    }
}
