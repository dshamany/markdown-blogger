use std::collections::HashMap;

use markdown;
use regex::Regex;
use reqwest::Client;
use serde_json::{json, Value};

fn cleanup_content(content: &str) -> String {
    content.trim_matches('"').replace("\n", "").to_string()
}

fn is_markdown_file(filename: &str) -> bool {
    let re_extention = Regex::new(r".md$").unwrap();
    re_extention.is_match(filename)
}

pub async fn get_markdown_document(url: &str) -> Result<String, String> {
    let client = Client::new();

    let response = client.get(url).send().await;

    match response {
        Ok(res) => {
            let valid_markdown = is_markdown_file(res.url().path().to_string().as_str());

            match valid_markdown {
                true => {
                    let content = res.text().await;

                    if let Ok(c) = content {
                        return Ok(c);
                    } else {
                        return Err("Failed to open markdown file".to_string());
                    }
                }
                false => Err("Invalid markdown file".to_string()),
            }
        }
        Err(_) => Err(format!("Failed to fetch markdown file from {}", url).to_string()),
    }
}

pub fn extract_n_match(regex: &Regex, haystack: &str, group_num: usize) -> Result<String, String> {
    let captures = regex.captures(haystack);

    match captures {
        Some(cap) => {
            let group_cap = cap.get(group_num);

            match group_cap {
                Some(c) => Ok(c.as_str().to_string()),
                None => Err("Could not find group".to_string()),
            }
        }
        None => Err("No Captures".to_string()),
    }
}

pub fn extract_all_matches(regex: &Regex, haystack: &str) -> Vec<String> {
    regex
        .find_iter(haystack)
        .map(|m| m.as_str().to_string())
        .collect()
}

pub fn regex_replace(regex: &Regex, replacement: &str, hayatack: &str) -> String {
    regex.replace(hayatack, replacement).to_string()
}

pub fn regex_replace_all(regex: &Regex, replacement: &str, hayatack: &str) -> String {
    regex.replace_all(hayatack, replacement).to_string()
}

fn extract_title_image(images: Vec<String>) -> String {
    if images.is_empty() {
        return "".to_string();
    }

    images[0].to_string()
}

fn extract_image_content(image_string: &str) -> (String, String) {
    if image_string == "" {
        return ("".to_string(), "".to_string());
    }
    let image_string: Vec<&str> = image_string.split("](").collect();
    let alt_text = image_string[0].strip_prefix("![").unwrap();
    let image_url = image_string[1].strip_suffix(")").unwrap();
    (image_url.to_string(), alt_text.to_string())
}

pub async fn to_value(from_markdown_url: &str) -> Result<Value, String> {
    let document = get_markdown_document(from_markdown_url).await;

    match document {
        Ok(content) => {
            let re_title = Regex::new(r"^# \w+.*").unwrap();
            let re_image = Regex::new(r"!\[.*\]\(.*\)").unwrap();
            let re_tags = Regex::new(r"#\w+").unwrap();

            // Inline HTML isn't supported by markdown lib
            let re_inline_html = Regex::new(r"<.*>.*</.>").unwrap();

            let title =
                extract_n_match(&re_title, content.as_str(), 0).unwrap_or("Untitled".to_string());
            let images = extract_all_matches(&re_image, content.as_str());
            let tags = extract_all_matches(&re_tags, content.as_str());

            // Remove extracted fields
            let content = regex_replace(&re_title, "", content.as_str());
            let content = regex_replace(&re_image, "", content.as_str());
            let content = regex_replace_all(&re_tags, "", content.as_str());
            let content = regex_replace_all(&re_inline_html, "", content.as_str());
            let title_image = extract_title_image(images);
            let (url, alt) = extract_image_content(title_image.as_str());

            let content = markdown::to_html(content.as_str());
            let content = cleanup_content(content.as_str());

            // This is the payload we use to populate the handlebars template
            let data = json!({
                "title": title,
                "image": {
                    "url": url,
                    "alt": alt
                },
                "tags": tags,
                "content": content
            });

            return Ok(data);
        }
        Err(e) => Err(e),
    }
}

#[allow(dead_code)]
pub async fn content_to_html_string(from_markdown_url: &str) -> Result<String, String> {
    let from_value = to_value(from_markdown_url).await?;

    Ok(cleanup_content(from_value["content"].to_string().as_str()))
}

#[allow(dead_code)]
pub fn value_to_html_string(content_from_value: Value) -> Result<String, String> {
    if content_from_value.is_string() {
        let value_to_string = content_from_value.to_string();
        Ok(cleanup_content(&value_to_string.as_str()))
    } else {
        Err("Value is not of type string".to_string())
    }
}

#[allow(dead_code)]
pub async fn to_raw_html(from_markdown_url: &str) -> Result<String, String> {
    let document = get_markdown_document(from_markdown_url).await;

    if let Ok(doc) = document {
        return Ok(markdown::to_html(doc.as_str()));
    } else {
        return Err("Failed to convert markdown to HTML".to_string());
    }
}

pub fn extract_by_pattern(pattern: &str, document: &str) -> Vec<String> {
    let re_links = Regex::new(pattern).unwrap();
    extract_all_matches(&re_links, document)
}

pub fn extract_link_components_to_hashmap(link: &String) -> HashMap<String, String> {
    let mut result = HashMap::<String, String>::new();

    let re_pieces = Regex::new(r"\[(.*)\]\((.*)\).*\[(.*)\]\((.*.md.*)\)").unwrap();
    let image_text = extract_n_match(&re_pieces, link.as_str(), 1);
    let image_url = extract_n_match(&re_pieces, link.as_str(), 2);
    let title = extract_n_match(&re_pieces, link.as_str(), 3);
    let url = extract_n_match(&re_pieces, link.as_str(), 4);

    // Extract the published date from the filename if it exists
    let re_date_published = Regex::new(r"(\d{4}).(\d{2}).(\d{2})").unwrap();
    let date_published = extract_n_match(&re_date_published, url.clone().unwrap().as_str(), 0);

    // Add placeholder image if the image_url is empty
    if let Ok(img) = image_url {
        if img.is_empty() {
            let placeholder =
            "http://icon-library.com/images/photo-placeholder-icon/photo-placeholder-icon-17.jpg";
            result.insert("image_url".to_string(), placeholder.to_string());
        } else {
            result.insert("image_url".to_string(), img);
        }
    }

    result.insert(
        "image_text".to_string(),
        image_text.unwrap_or("None".to_string()),
    );
    result.insert("title".to_string(), title.unwrap_or("".to_string()));
    result.insert("url".to_string(), url.unwrap_or("".to_string()));
    result.insert(
        "published".to_string(),
        date_published.unwrap_or("".to_string()),
    );
    result
}

pub fn links_as_objects(links: &Vec<String>) -> Vec<HashMap<String, String>> {
    links
        .iter()
        .map(|link| extract_link_components_to_hashmap(link))
        .collect::<Vec<_>>()
}
