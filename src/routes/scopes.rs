use actix_web::{web, Scope};

use crate::handlers::{api, views};

pub fn root() -> Scope {
    web::scope("").service(views::index::start)
}

pub fn healthcheck() -> Scope {
    web::scope("/healthcheck").service(api::health::check)
}

pub fn views() -> Scope {
    web::scope("/views")
        .service(views::index::home)
        .service(views::index::md2html)
        .service(views::index::md2template)
}

pub fn apis() -> Scope {
    web::scope("/api/v1")
    // .service(api::database::create_item)
    // .service(api::database::read_all_items)
    // .service(api::database::read_item_by_attribute)
    // .service(api::database::read_item_by_id)
    // .service(api::database::update_item_by_id)
    // .service(api::database::delete_item_by_id)
}
