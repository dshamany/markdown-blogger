FROM rust:1.73 as build

# create a new empty shell project
RUN USER=root cargo new --bin app
WORKDIR /app

# copy over manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache our dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy our source tree
COPY ./src ./src

# build for release
RUN rm ./target/release/deps/app*
RUN cargo build --release

# our new base
FROM rust:1.73

# copy HTML templates
COPY ./static ./static

# build the artifact
COPY --from=build /app/target/release/app .

# set the startup command to run our binary
CMD ["./app"]
